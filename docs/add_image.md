[Menu principal](Extra/home)|
|---|

# Como adicionar imagens no Gitlab

---
**Acesso Rápido**

---

* [Direto pelo Edit da Página](Extra/add_image#edit)
* [Adicionando por um Editor de Texto](Extra/add_image#vscode)
* [Formas de Adicionar imagens em Markdown](Extra/add_image#adding)
* [Visualizacão das Imagens](Extra/add_image#organize)

---

<a name="edit"></a>
## **Direto pelo `Edit` da Página:**


* Vamos até a Wiki desejada e procuramos a página que desejamos colocar a imagem

<img src="../img/paginawiki.png">
<img src="">

* Agora clicamos no botão de `Edit` que aparece encima da página,como mostrado abaixo

![editwiki](../img/edit_do_wiki.png)

* Clicamos na opção `Attach a file` e selecionamos a imagem do nosso computador que queremos colocar

![attach](../img/attachafile.png)

![addpic](../img/addpic.png)

* Espere ele adicionar a imagem como um imagem transformada em um sistema de markdown

![mdpic](../img/markdownpic.png)


* Selecione `Save Changes` e **Pronto**, A imagem será exibida na página

![final](../img/finalpicattach.png)


<a name="vscode"></a>
## **Adicionando em um Editor de Texto:**

* Em editores, podemos colocar imagens em **diretórios que ficarão ocultos** para as pessoas que irão ler mas podemos manipular as imagens que estarão armazenadas

* Isso serve para quando trocarmos o local dos arquivos da Wiki para outro Repositório, nao termos muito trabalho em visualizar as imagens

### Como funciona:

* Primeiro criamos um Diretorio na nossa Wiki e Adicionamos uma imagem nele

---

<img src="../img/vscode_folder_image.png">

---

* Agora essa imagem esta dentro de um Diretorio Oculto que so em um editor de texto como o VScode voce consegue ver, onde a unica coisa que devemos fazer agora e referenciar no Markdown a imagem que se encontra nesse Repositorio, que nos leva ao proximo topico abaixo



<a name="adding"></a>
## Formas de Adicionar imagens em Markdown

* Existem algumas formas de adicionar imagens no Markdown:
    * [Por link externo](Extra/add_image#extern)
    * [Por link interno](Extra/add_image#oculto)
    * [por html interno](Extra/add_imagem#html)

<a name="extern"></a>
**Por link Externo**

iremos usar a seguine estrutura para trazer imagens externar ao Markdown:

```
![nome desejado da imagem](link da imagem)
```

Com essa estruturação podemos pegar um link qualquer de uma imagem e adicionar, colocando um nome para a imagem em si

`EXEMPLO:` 

* Em uma imagem na internet selecione `copiar o endereço da imagem`

* Coloque o link e o nome da imagem como abaixo

```
![Terminal](https://cdn.macpaw.com/uploads/images/terminal1234.png)
```

* vai aparecer como abaixo: 

![Terminal](https://cdn.macpaw.com/uploads/images/terminal1234.png)


<a name="oculto"></a>
**Por link interno**

* Se voce estiver trabalhando com diretorios de imagens em um editor de texto como o vscode, podemos simplesmente chamar o arquivo de seu diretorio 

* por link se usa uma estrutura como abaixo para chamar a imagem
```
![nome da imagem](caminho/para/imagem)
```

Exemplo: 

* Descobrimos onde esta a nossa imagem:

<img src="../img/ocult_image_example.png">

* Colocamos o caminho para a imagem do diretorio como abaixo 

```
![](../img/terminal.png)
```

* Colocamos `..` em todo diretorio que nao for interno do Reposiorio, entao como o diretorio imagens e interno, devemos dizer todos os diretorio ate a imagem e a extensao dela

* Abaixo fica a imagem em si

![](../img/terminal.png)


<a name="html"></a>
**Por html interno**

* por html usamos a tag epecial do html para exportar imagem, dizendo como fonte o local da imagem e o titulo dela

```
<img src="caminho/para/imagem" tittle="nome da imagem">
```
* se desejamos **Transformar markdown em PDF**, devemos sempre chamar as imagens como `tag` 
* se desejamos **Fazer resize de imagens**, devemos usar tambem as tags, porque fica mais facil de usar, foi explicado mais a fundo [Aqui](add_image#organize)

`EXEMPLO:`

* Descobrimos onde esta a nossa imagem:

<img src="../img/ocult_image_example.png">

* Colocamos o caminho para a imagem do diretorio como abaixo 

```
<img src="../img/terminal.png">
```

* Colocamos `..` em todo diretorio que nao for interno do Reposiorio, entao como o diretorio imagens e interno, devemos dizer todos os diretorio ate a imagem e a extensao dela

* Abaixo fica a imagem em si

<img src="../img/terminal.png">


<a name="organize"></a>
## Visualização das Imagens

* Podemos redefinir o tamanho e centralizar as imagens usando tags

```
<p align="center">
    <img src="imagem"
 height="300" tittle="nome imagem">
</p>
```

* `align=""` : e uma forma de orientar aonde a imagem vai ficar 
* `height=""` : define o tamanho que a imagem vai ficar na wiki

Exemplo:

* **CENTRALIZANDO**

```
<p align="center">
    <img src="../img/terminal.png" height="100">
</p>
```
<p align="center">
    <img src="../img/terminal.png" height="100">
</p>

* **Diversos tamanhos**

```
<img src="../img/terminal.png" height="20">
<img src="../img/terminal.png" height="50">
<img src="../img/terminal.png" height="100">
<img src="../img/terminal.png" height="200">
```

<img src="../img/terminal.png" height="20">
<img src="../img/terminal.png" height="50">
<img src="../img/terminal.png" height="100">
<img src="../img/terminal.png" height="200">